{
  description = "Rust Development Overlay";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url  = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, fenix, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlays.default ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        toolchain = pkgs.fenix.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-PJWkZkDThJgAx75N9+HDl8jd6F87KrfsNwlvwPbcIog=";
        };

        buildInputs = with pkgs; [
          openssl
          libuv
          systemd

          # rust
          toolchain
        ];

        nativeBuildInputs = with pkgs; [ pkgconfig fd cargo-espflash];
        ldLibPath = pkgs.lib.makeLibraryPath buildInputs;
      in
        {
          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer-nightly ];
            nativeBuildInputs = nativeBuildInputs;

            shellHook = ''
            echo "Loaded devshell"
            export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:${pkgs.lib.makeLibraryPath nativeBuildInputs}:$LD_LIBRARY_PATH"
          '';
          };
        }
    );
}
